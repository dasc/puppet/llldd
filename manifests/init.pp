class llldd (
    String $package_ensure,
    String $service_ensure,
    Boolean $service_enable,
    Hash $parts,
    String $mcast_bind_addr,
    String $mcast_ip,
    Integer $dpush_update_interval,
    Integer $shmlock_max_pages,
    Integer $llsocket_min_rcvbuf,
    Boolean $enable_framelog,
    Integer $logger_ring_size,
    Boolean $enable_verbose,
    String $llpipedir,
    String $default_shm_dir,
    String $default_frametype,
) {
    package {'llldd': ensure => $package_ensure, }
    package {'llldd-config-llo': ensure => $package_ensure, }
    package {'llldd-systemd': ensure => $package_ensure, }

    file { '/etc/llldd':
	ensure => 'directory',
	owner => 'root',
	group => 'root',
	mode => '0755',
	require => Package['llldd'],
    }

    file {'/etc/llldd/llshm_config':
        ensure => 'file',
	owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp('llldd/llshm_config.epp', {
	    parts => $parts,
	}),
	require => Package['llldd'],
	notify => [
	    Exec['/usr/bin/llldd-helper --force generate-master'],
	    Service['llldd-master.target'],
	],
    }
    file { '/etc/sysconfig/llldd':
	ensure => 'file',
	owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp('llldd/llldd.sysconfig.epp', {
	    parts => $parts,
	    mcast_bind_addr => $mcast_bind_addr,
	    mcast_ip => $mcast_ip,
	    dpush_update_interval => $dpush_update_interval,
	    shmlock_max_pages => $shmlock_max_pages,
	    llsocket_min_rcvbuf => $llsocket_min_rcvbuf,
	    enable_framelog => $enable_framelog,
	    enable_verbose => $enable_verbose,
	    logger_ring_size => $logger_ring_size,
	    llpipedir => $llpipedir,
	    default_frametype => $default_frametype,
	    default_shm_dir => $default_shm_dir,
	}),
	notify => [ Service['llldd-master.target'], ],
	require => Package['llldd'],
    }
    exec {'llldd daemon-reload':
        command => '/usr/bin/systemctl daemon-reload',
        refreshonly => true,
    } -> Service <| |>

    exec { '/usr/bin/llldd-helper --force generate-master':
	refreshonly => true,
	notify => Exec['llldd daemon-reload'],
    }
    service { 'llldd-master.target':
	ensure => $service_ensure,
	enable => $service_enable,
	require => Package['llldd-systemd'],
    }
}
